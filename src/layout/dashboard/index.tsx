import NavBar from "../NavBar"
import Header from "../Header"
import Footer from "../Footer"
import { isAuthenticated } from "../../redux/selectors/auth.selector"
import { useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { NAME_ROUTE } from "../../routes/path"
import { useEffect } from "react"

const DashboardLayout = () => {
  const isAuth = useSelector(isAuthenticated)
  const navigate = useNavigate()

  useEffect(()=>{
      if(!isAuth){
        navigate(NAME_ROUTE.AUTH.DEFAULT_PATH + "/" + NAME_ROUTE.AUTH.LOGIN)
      }
  },[isAuth, navigate])

  return (
    <>
        <Header/>
        <NavBar/>
        <Footer/>
    </>
  )
}

export default DashboardLayout