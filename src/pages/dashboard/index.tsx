
import { isAuthenticated } from "../../redux/selectors/auth.selector"
import { useSelector } from "react-redux"
import { Outlet, useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { NAME_ROUTE } from "../../routes/path";

const MainboardLayout = () => {
    const isAuth = useSelector(isAuthenticated)
    const navigate = useNavigate()

    useEffect(()=>{
        if(isAuth){
            navigate(NAME_ROUTE.DASHBOARD.DEFAULT_PATH)
        }
    },[isAuth, navigate])

    return (
      <>
      MainboardLayout
      </>
    )
  }
  
  export default MainboardLayout